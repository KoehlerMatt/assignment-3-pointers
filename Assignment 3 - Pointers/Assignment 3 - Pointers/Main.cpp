
// Assignment 3 - Pointers
// Matt Koehler


#include <iostream>
#include <conio.h>

using namespace std;

// TODO: Implement the "SwapIntegers" function

//Function Prototype
void SwapIntegers(int *pFirst,int *pSecond);

// Do not modify the main function!
int main()
{
	int first = 0;
	int second = 0;

	cout << "Enter the first integer: ";
	cin >> first;

	cout << "Enter the second integer: ";
	cin >> second;

	cout << "\nYou entered:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	SwapIntegers(&first, &second);

	cout << "\nAfter swapping:\n";
	cout << "first: " << first << "\n";
	cout << "second: " << second << "\n";

	cout << "\nPress any key to quit.";

	_getch();
	return 0;
}

void SwapIntegers(int *pFirst, int *pSecond) 
{
	int x = *pFirst;
	*pFirst = *pSecond;
	*pSecond = x;
}